// This file is used to register all your cloud functions in GCP.
// DO NOT EDIT/DELETE THIS, UNLESS YOU KNOW WHAT YOU ARE DOING.

exports.k6gcpnode = require("./k6gcpnode.js").handler;
exports.subyklambdafile = require("./suby/klambdafile.js").handler;